package fr.mc2i.democicdformationdevops;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DemocicdformationdevopsApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void greetings_works() throws Exception {
		this.mockMvc.perform(get("/greeting"))
				.andExpect(status().isOk())
				.andExpect(content().string("Hello, World!"));
	}

	@Test
	void add_works() throws Exception {
		this.mockMvc.perform(get("/add?a=1&b=2"))
				.andExpect(status().isOk())
				.andExpect(content().string("3"));
	}

}
