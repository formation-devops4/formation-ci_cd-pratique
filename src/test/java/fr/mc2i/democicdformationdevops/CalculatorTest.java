package fr.mc2i.democicdformationdevops;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Test
    void should_add_correctly() {
        Assertions.assertEquals(7, calculator.add(3, 4));
    }

    @Test
    void should_sub_correctly() {
        Assertions.assertEquals(2, calculator.sub(8, 6));
    }

    @Test
    void should_mul_correctly() {
        Assertions.assertEquals(10, calculator.mul(2, 5));
    }

    @Test
    void should_div_correctly() {
        Assertions.assertEquals(3, calculator.div(6, 2));
    }

}
