package fr.mc2i.democicdformationdevops;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Component
@RestController
public class GreetingController {

    @Autowired
    private Calculator calculator;

    @GetMapping(value = "/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name) {
        return String.format("Hello, %s!", name);
    }

    @GetMapping(value = "/add")
    public int add(@RequestParam(name = "a") int a, @RequestParam(name = "b") int b) {
        return calculator.add(a, b);
    }

}
