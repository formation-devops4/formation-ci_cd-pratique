package fr.mc2i.democicdformationdevops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocicdformationdevopsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemocicdformationdevopsApplication.class, args);
	}

}
